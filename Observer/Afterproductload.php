<?php

namespace Wm21w\CustomMeta\Observer;

class Afterproductload implements \Magento\Framework\Event\ObserverInterface
{

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();
        $metaDescription = $product->getName() . ' ➤ ' . $product->getNrPorownawcze() . ' ➤ ' . $product->getDodatkoweNrPorownawcze();
        $product->setMetaDescription($metaDescription);
        $product->setIsMetaApplied(true);

        return $this;
    }
}